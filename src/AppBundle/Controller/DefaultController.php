<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Unirest;

class DefaultController extends Controller
{
    const STATUS_OK = 200;

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $response = Unirest\Request::get('http://localhost:8001/question');
        $questions = $response->body;
        $response = Unirest\Request::get('http://localhost:8001/answer');
        $answers = $response->body;

        forEach ($questions as $question){
            $question->respostes = [];
            $idQuestion = $question->id;
            forEach ($answers as $answer){
                if($answer->id_question === $idQuestion){
                    array_push($question->respostes, $answer);
                }
            }
        }

        return $this->render('default/index.html.twig', [
            'questions' => $questions,
        ]);
    }

    /**
     * @Route("/add", name="addpage")
     */
    public function addAction(Request $request)
    {
        if($request->isMethod('post')){
            $text = $request->request->get('question');
            $correctAnswer = $request->request->get('correct_answer');
            $otherAnswers = [ $request->request->get('first_answer') , $request->request->get('second_answer') , $request->request->get('third_answer')];

            //guardamos pregunta
            $headers = array('Accept' => 'application/json');
            $query = array('text' => $text);
            $response = Unirest\Request::post('http://localhost:8001/question/', $headers, $query);

            //consultamos el id de la pregunta
            $response = Unirest\Request::get('http://localhost:8001/question');
            $questions = $response->body;
            forEach ($questions as $question){
                if($question->text === $text){
                    $questionId = $question->id;
                }
            }

            //guardamos respuestas
            $headers = array('Accept' => 'application/json');
            $query = array('text' => $correctAnswer, 'id_question' => $questionId, 'is_correct' => 1 );
            $response = Unirest\Request::post('http://localhost:8001/answer/', $headers, $query);
            forEach($otherAnswers as $answer){
                $query = array('text' => $answer, 'id_question' => $questionId, 'is_correct' => 0 );
                $response = Unirest\Request::post('http://localhost:8001/answer/', $headers, $query);
            }

            if($response->code === self::STATUS_OK){
                $url = $this->generateUrl('homepage');
                $response = new RedirectResponse($url);
                return $response;
            }
        }
        return $this->render('default/form.html.twig', [
            'action' => '/add'
        ]);
    }

    /**
     * @Route("/delete/{id}", name="deletepage")
     */
    public function deleteAction(Request $request, string $id)
    {
        //Unirest\Request::delete($url, $headers = array(), $body = null)

        $response = Unirest\Request::get('http://localhost:8001/question');
        $questions = $response->body;
        $response = Unirest\Request::get('http://localhost:8001/answer');
        $answers = $response->body;

        //esborrem les respostes primer
        $respostes = [];
        forEach ($answers as $answer){
            if($answer->id_question == $id){
                Unirest\Request::delete('http://localhost:8001/answer/' . $answer->id);
            }
        }
        //i despres la pregunta ja que es clau foranea
        $response = Unirest\Request::delete('http://localhost:8001/question/' . $id);

        if($response->code === self::STATUS_OK){
            $url = $this->generateUrl('homepage');
            $response = new RedirectResponse($url);
            return $response;
        }
    }
}
